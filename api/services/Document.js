// Require library
var xl = require('excel4node');
var fs = require('fs');
module.exports = {
  excel: async (startDate, endDate, columns, repeated, callback) => {
    var limit = 100;
    var emailHeader = 'Correo electrónico';
    if (columns) {
      columns.sort((x, y) => {
        return x == emailHeader ? -1 : y == emailHeader ? 1 : 0;
      });
    }
    var cols = columns || sails.config.report.columns;
    var register = 'Fecha de registro';
    var foundRegister = cols.find(c => {
      return c == register;
    });
    columns = '"' + cols.join('","') + '"';
    if (!foundRegister) {
      columns = columns + `,"${register}"`;
    }
    if (!repeated && cols[0] == emailHeader) {
      columns = 'DISTINCT ' + columns;
    }
    var QUERY_VIEW = `SELECT ${columns} FROM public.full_lead
                      WHERE "${register}" >= $1 AND "${register}" < $2
                      ORDER BY "${register}" LIMIT 99999`;

    console.log('Getting leads from:', startDate, '- to:', endDate);
    var leads = await sails.sendNativeQuery(QUERY_VIEW, [startDate, endDate]);
    console.log('Leads found:', leads.rowCount);

    // Create a new instance of a Workbook class
    var wb = new xl.Workbook();
    // Add Worksheets to the workbook
    var ws = wb.addWorksheet('Sheet 1');

    // Fill report headers
    _.each(cols, (element, index) => {
      ws.cell(1, index + 1).string(element);
    });

    sails.async.eachOfLimit(
      leads.rows,
      limit,
      (lead, index, cbb) => {
        // Fill report content
        _.each(cols, (e, i) => {
          if (lead[e] && typeof lead[e] !== 'string') {
            lead[e] = lead[e].toLocaleString('es');
          }
          ws.cell(index + 2, i + 1).string(lead[e] || '');
        });

        setTimeout(cbb, 0);
      },
      () => {
        wb.write(sails.config.report.path);
        console.log('Excel completed');
        var response = {
          count: leads.rowCount,
          path: sails.config.report.path
        };
        callback(null, response);
      }
    );
  },

  delete: filePath => {
    var path = filePath || sails.config.report.path;
    fs.unlink(path, err => {
      if (err) {
        console.log(path, ' wasn\'t deleted');
      } else {
        console.log(path, ' was deleted');
      }
    });
  },

  send: async hour => {
    let reports = await Report.find({
      hour: hour,
      active: true
    });
    var now = new Date();
    var yesterday = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate() - 1,
      hour,
      0,
      0,
      0
    );
    var today = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate(),
      hour,
      0,
      0,
      0
    );
    reports.forEach(report => {
      var columns = report.columns.split(',').map(c => {
        return c.trim();
      });
      Document.excel(
        yesterday,
        today,
        columns,
        report.repeatedData,
        (err, data) => {
          if (err) {
            console.log(err);
          } else {
            var subject = report.subject || 'Reporte ' + report.title;
            var message = `Reporte del ${yesterday.toLocaleString('es')}
            al ${today.toLocaleString('es')}.
            Total de leads nuevos: ${data.count}.`;
            if (!report.repeatedData) {
              message =
                message +
                ' Nota: El reporte omite los leads repetidos, basado en su correo.';
            }
            var attachments =
              data.count > 0 ? [{
                path: data.path
              }] :
              null;
            var emails = report.emails.split(',').map(c => {
              return c.trim();
            });
            Email.send(subject, message, attachments, emails, null, () => {
              Document.delete(data.path);
            });
          }
        }
      );
    });
  },

  toBase64: file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  }),
};
