/**
 * Card.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

 module.exports = {

   attributes: {
     title: {
       type: 'string',
       required: true
     },
     description: {
       type: 'string'
     },
     benefits: {
       type: 'string'
     },
     investment: {
       type: 'string'
     },
     cat: {
       type: 'string'
     },
     mount: {
       type: 'number',
       defaultsTo: 0
     },
     image: {
       type: 'string',
       required: true
     },
     url: {
       type: 'string',
       required: true
     },
     active: {
       type: 'boolean',
       defaultsTo: true
     },
     order: {
       type: 'number',
       defaultsTo: 0
     },

     // Foreign key
     cardType: {
       model: 'cardType',
       columnName: 'cardTypeId'
     },

     // Associations
     likes: {
       collection: 'like',
       via: 'card'
     },
     dislikes: {
       collection: 'dislike',
       via: 'card'
     },

   }

 };
