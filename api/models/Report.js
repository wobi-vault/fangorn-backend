/**
 * Report.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    title: {
      type: 'string',
      required: true
    },
    description: {
      type: 'string',
      allowNull: true
    },
    hour: {
      type: 'number',
      required: true
    },
    columns: {
      type: 'string',
      required: true
    },
    emails: {
      type: 'string',
      required: true
    },
    subject: {
      type: 'string',
      allowNull: true
    },
    repeatedData: {
      type: 'boolean',
      defaultsTo: false
    },
    active: {
      type: 'boolean',
      defaultsTo: true
    }

  },

};
