/**
 * CardController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  update: async (req, res) => {
    let params = req.allParams();
    const id = req.param('id');
    params = _.omit(params, 'likes', 'dislikes', 'cardType');

    const card = await Card.update({ id }).set(params).fetch();

    res.ok(card);
  },

  order: (req, res) => {
    const params = req.allParams();

    if (params.cards) {
      async.eachOf(
        params.cards,
        async (card, index, callback) => {
          await Card.updateOne(card).set({ order: index + 1 });
          callback();
        },
        (err, results) => {
          return res.ok();
        }
      );
    } else {
      return res.badRequest();
    }
  }

};
