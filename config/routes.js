/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  'POST /report/:id/resend': {
    controller: 'Report',
    action: 'resend'
  },
  'GET /report/columns': {
    controller: 'Report',
    action: 'columns'
  },
  'POST /card/order': {
    controller: 'Card',
    action: 'order'
  }

};
